/*
  Warnings:

  - Changed the type of `publishedAt` on the `Movie` table. No cast exists, the column would be dropped and recreated, which cannot be done if there is data, since the column is required.

*/
-- AlterTable
ALTER TABLE "Movie" DROP COLUMN "publishedAt",
ADD COLUMN     "publishedAt" INTEGER NOT NULL;

-- CreateIndex
CREATE UNIQUE INDEX "Movie_originalName_publishedAt_directorId_key" ON "Movie"("originalName", "publishedAt", "directorId");
