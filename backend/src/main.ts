import { HttpAdapterHost, NestFactory } from '@nestjs/core';
import { ConfigService } from '@nestjs/config';
import { AppModule } from './app.module';
import { ValidationPipe } from '@nestjs/common';
import {
  DocumentBuilder,
  SwaggerCustomOptions,
  SwaggerModule,
} from '@nestjs/swagger';
import { PrismaClientExceptionFilter } from './prisma/prisma-client-exception.filter';

async function bootstrap(): Promise<void> {
  const app = await NestFactory.create(AppModule);
  const configService = app.get(ConfigService);

  app.enableCors();

  app.useGlobalPipes(
    new ValidationPipe({
      transform: true,
    }),
  );

  const { httpAdapter } = app.get(HttpAdapterHost);
  app.useGlobalFilters(new PrismaClientExceptionFilter(httpAdapter));

  const swaggerDocConfig = new DocumentBuilder()
    .setTitle('Movie Base API')
    .setVersion('1.0')
    .addTag('movies')
    .addTag('directors')
    .addTag('categories')
    .build();
  const swaggerDoc = SwaggerModule.createDocument(app, swaggerDocConfig);
  const swaggerOptions: SwaggerCustomOptions = {
    customSiteTitle: 'Movie Base API Docs',
  };
  SwaggerModule.setup('/', app, swaggerDoc, swaggerOptions);

  const port = configService.get<number>('PORT') | 4000;
  console.log(`NestJS server is listening on: http://localhost:${port}`);
  await app.listen(port);
}

bootstrap();
