import { IMovieDto } from './movieDto';

export interface ICategoryDto {
  id: string;
  name: string;
  movies: IMovieDto[];
}
