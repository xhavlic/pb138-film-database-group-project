import { IMovieDto } from './movieDto';

export interface IDirectorDto {
  id: string;
  name: string;
  surname: string;
  birthDate: Date;
  movies: IMovieDto[];
}
