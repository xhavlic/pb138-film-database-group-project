import { ICategoryDto } from './categoryDto';
import { IDirectorDto } from './directorDto';
import { IMovieDto } from './movieDto';

export { ICategoryDto, IDirectorDto, IMovieDto };
