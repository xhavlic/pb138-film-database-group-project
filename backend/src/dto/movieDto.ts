import { ICategoryDto } from './categoryDto';
import { IDirectorDto } from './directorDto';

export interface IMovieDto {
  id: string;
  name: string;
  originalName: string;
  intro: string;
  picture?: string;
  publishedAt: Date;
  runTimeMinutes: number;
  director: IDirectorDto;
  categories: ICategoryDto[];
}
