import {
  CanActivate,
  ExecutionContext,
  Injectable,
  UnauthorizedException,
} from '@nestjs/common';
import { expressJwtSecret } from 'jwks-rsa';
import { promisify } from 'util';
import { ConfigService } from '@nestjs/config';

@Injectable()
export class AuthorizationGuard implements CanActivate {
  private readonly AUTH_AUDIENCE: string;
  private readonly AUTH_DOMAIN: string;

  constructor(private configService: ConfigService) {
    this.AUTH_AUDIENCE = this.configService.get('AUTH_AUDIENCE');
    this.AUTH_DOMAIN = this.configService.get('AUTH_DOMAIN');
  }
  async canActivate(context: ExecutionContext): Promise<boolean> {
    const { expressjwt: jwt } = require("express-jwt");
    const req = context.getArgByIndex(0);
    const res = context.getArgByIndex(1);
    const checkJwt = promisify(
      jwt({
        secret: expressJwtSecret({
          cache: true,
          rateLimit: true,
          jwksRequestsPerMinute: 5,
          jwksUri: `${this.AUTH_DOMAIN}.well-known/jwks.json`,
        }),
        audience: this.AUTH_AUDIENCE,
        issuer: this.AUTH_DOMAIN,
        algorithms: ['RS256'],
      }),
    );

    try {
      await checkJwt(req, res);
      return true;
    } catch (error) {
      throw new UnauthorizedException(error);
    }
  }
}
