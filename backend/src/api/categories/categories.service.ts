import { Injectable } from '@nestjs/common';
import { PrismaService } from 'src/prisma/prisma.service';
import { CreateCategoryDto } from './dto/create-category.dto';
import { UpdateCategoryDto } from './dto/update-category.dto';
import { plainToClass } from 'class-transformer';
import { CategoryEntity } from './entities/category.entity';
import { Category } from '@prisma/client';

@Injectable()
export class CategoriesService {
  constructor(private prisma: PrismaService) {}

  async create(createCategoryDto: CreateCategoryDto): Promise<Category> {
    const category = plainToClass(CategoryEntity, createCategoryDto);
    return await this.prisma.category.create({
      data: category,
    });
  }

  async findAll(): Promise<Category[]> {
    return await this.prisma.category.findMany({
      include: {
        movies: {
          orderBy:{
            name: 'asc',
          }
        }
      },
      orderBy: {
        name: 'asc',
      }
    });
  }

  async findOne(id: string): Promise<Category> {
    return await this.prisma.category.findUnique({
      where: {
        id,
      },
    });
  }

  async update(
    id: string,
    updateCategoryDto: UpdateCategoryDto,
  ): Promise<Category> {
    const updatedCategory = {
      ...(await this.findOne(id)),
      ...plainToClass(CategoryEntity, updateCategoryDto),
    };
    return await this.prisma.category.update({
      where: {
        id,
      },
      data: {
        ...updatedCategory,
      },
    });
  }

  async remove(id: string): Promise<Category> {
    return await this.prisma.category.update({
      where: {
        id,
      },
      data: {
        deletedAt: new Date(),
      },
    });
  }
}
