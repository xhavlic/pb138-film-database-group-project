import { Injectable } from '@nestjs/common';
import { CreateDirectorDto } from './dto/create-director.dto';
import { UpdateDirectorDto } from './dto/update-director.dto';
import { PrismaService } from '../../prisma/prisma.service';
import { plainToClass } from 'class-transformer';
import { DirectorEntity } from './entities/director.entity';
import { Director } from '@prisma/client';

@Injectable()
export class DirectorsService {
  constructor(private prisma: PrismaService) {}

  async create(createDirectorDto: CreateDirectorDto): Promise<Director> {
    const director = plainToClass(DirectorEntity, createDirectorDto);
    return await this.prisma.director.create({
      data: director,
    });
  }

  async findAll(): Promise<Director[]> {
    return await this.prisma.director.findMany();
  }

  async findOne(id: string): Promise<Director> {
    return await this.prisma.director.findUnique({
      where: {
        id,
      },
      include: {
        movies: true,
      },
    });
  }

  async update(
    id: string,
    updateDirectorDto: UpdateDirectorDto,
  ): Promise<Director> {
    const updatedDirector = {
      ...(await this.findOne(id)),
      ...plainToClass(DirectorEntity, updateDirectorDto),
    };
    return await this.prisma.director.update({
      where: {
        id,
      },
      data: {
        ...updatedDirector,
      },
    });
  }

  async remove(id: string): Promise<Director> {
    const directorMovie = await this.prisma.movie.findFirst({
      where: {
        directorId: id,
        deletedAt: null,
      },
      select: {
        id: true,
      },
    });

    if (directorMovie === undefined) {
      return await this.prisma.director.update({
        where: {
          id,
        },
        data: {
          deletedAt: new Date(),
        },
      });
    }

    return await this.findOne(id);
  }
}
