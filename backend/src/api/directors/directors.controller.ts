import {
  Controller,
  Get,
  Post,
  Body,
  Patch,
  Param,
  Delete,
  UseGuards,
} from '@nestjs/common';
import { ApiTags } from '@nestjs/swagger';
import { DirectorsService } from './directors.service';
import { CreateDirectorDto } from './dto/create-director.dto';
import { UpdateDirectorDto } from './dto/update-director.dto';
import { DirectorEntity } from './entities/director.entity';
import { AuthorizationGuard } from '../../authorization/authorization.guard';

@ApiTags('directors')
@Controller('directors')
export class DirectorsController {
  constructor(private readonly directorsService: DirectorsService) {}

  @UseGuards(AuthorizationGuard)
  @Post()
  async create(
    @Body() createDirectorDto: CreateDirectorDto,
  ): Promise<DirectorEntity> {
    return await this.directorsService.create(createDirectorDto);
  }

  @Get()
  async findAll(): Promise<DirectorEntity[]> {
    return await this.directorsService.findAll();
  }

  @Get(':id')
  async findOne(@Param('id') id: string): Promise<DirectorEntity> {
    return await this.directorsService.findOne(id);
  }

  @UseGuards(AuthorizationGuard)
  @Patch(':id')
  async update(
    @Param('id') id: string,
    @Body() updateDirectorDto: UpdateDirectorDto,
  ): Promise<DirectorEntity> {
    return await this.directorsService.update(id, updateDirectorDto);
  }

  @UseGuards(AuthorizationGuard)
  @Delete(':id')
  async remove(@Param('id') id: string): Promise<DirectorEntity> {
    return await this.directorsService.remove(id);
  }
}
