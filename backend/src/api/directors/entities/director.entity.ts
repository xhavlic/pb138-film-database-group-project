import { ApiHideProperty } from '@nestjs/swagger';
import { Director } from '@prisma/client';

export class DirectorEntity implements Director {
  id: string;

  @ApiHideProperty()
  createdAt: Date;

  @ApiHideProperty()
  updatedAt: Date;

  @ApiHideProperty()
  deletedAt: Date;

  name: string;

  surname: string;

  birthDate: Date | null;
}
