import {
  IsNotEmpty,
  IsNumber,
  IsOptional,
  IsString,
  Min,
} from 'class-validator';

export class CreateMovieDto {
  @IsNotEmpty()
  @IsString()
  readonly name: string;

  @IsNotEmpty()
  @IsString()
  readonly originalName: string;

  @IsNotEmpty()
  @IsString()
  readonly intro: string;

  @IsNotEmpty()
  @IsOptional()
  @IsString()
  readonly picture?: string;

  @IsNotEmpty()
  @IsNumber()
  @Min(1878)
  readonly publishedAt: number;

  @IsNotEmpty()
  @IsString()
  readonly directorId: string;

  @IsNotEmpty()
  @IsNumber()
  @Min(1)
  readonly runTimeMinutes: number;
}
