import { ApiPropertyOptional } from "@nestjs/swagger";
import { Transform, Type } from "class-transformer";
import { IsOptional, IsPositive, IsString, IsUUID, Min, ValidateNested } from "class-validator";

class Runtime {
  @Transform(({ value }) => +value)
  @IsOptional()
  @Min(0)
  @ApiPropertyOptional({name: 'runtime[gte]'})
  gte?: number;

  @Transform(({ value }) => +value)
  @IsOptional()
  @Min(0)
  @ApiPropertyOptional({name: 'runtime[lte]'})
  lte?: number;
}

class Published {
  @Transform(({ value }) => +value)
  @IsOptional()
  @Min(1878)
  @ApiPropertyOptional({name: 'published[gte]'})
  gte?: number;

  @Transform(({ value }) => +value)
  @IsOptional()
  @Min(1878)
  @ApiPropertyOptional({name: 'published[lte]'})
  lte?: number;
}

export class MoviesQuery {
  @Transform(({ value }) => +value)
  @IsOptional()
  @IsPositive()
  @ApiPropertyOptional({name: 'mostRecentCnt'})
  mostRecentCnt?: number;

  @IsOptional()
  @IsString()
  @ApiPropertyOptional({name: 'name'})
  name?: string;

  @IsOptional()
  @IsString()
  @ApiPropertyOptional({name: 'origName'})
  origName?: string;

  @ValidateNested({ each: true })
  @Type(() => Runtime)
  @IsOptional()
  @ApiPropertyOptional({ type: () => Runtime })
  runtime?: Runtime;

  @ValidateNested({ each: true })
  @Type(() => Published)
  @IsOptional()
  @ApiPropertyOptional({ type: () => Published })
  published?: Published;

  @IsOptional()
  @IsUUID(undefined, { each: true })
  @ApiPropertyOptional({
    name: 'categoryIds',
    example: [
      '9322c384-fd8e-4a13-80cd-1cbd1ef95ba8',
      '986dcaf4-c1ea-4218-b6b4-e4fd95a3c28e',
    ],
  })
  categoryIds?: string[];

  @IsOptional()
  @IsUUID(undefined, { each: true })
  @ApiPropertyOptional({
    name: 'directorIds',
    example: [
      '9322c384-fd8e-4a13-80cd-1cbd1ef95ba8',
      '986dcaf4-c1ea-4218-b6b4-e4fd95a3c28e',
    ],
  })
  directorIds?: string[];
};
