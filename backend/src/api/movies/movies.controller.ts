import {
  Controller,
  Get,
  Post,
  Body,
  Patch,
  Param,
  Delete,
  Query, UseGuards,
} from '@nestjs/common';
import { ApiOperation, ApiTags } from '@nestjs/swagger';
import { MoviesService } from './movies.service';
import { CreateMovieDto } from './dto/create-movie.dto';
import { UpdateMovieDto } from './dto/update-movie.dto';
import { MovieEntity } from './entities/movie.entity';
import { MoviesQuery } from './dto/movies-query';
import { AuthorizationGuard } from '../../authorization/authorization.guard';

@ApiTags('movies')
@Controller('movies')
export class MoviesController {
  constructor(private readonly moviesService: MoviesService) {}

  @UseGuards(AuthorizationGuard)
  @Post()
  async create(@Body() createMovieDto: CreateMovieDto): Promise<MovieEntity> {
    return await this.moviesService.create(createMovieDto);
  }

  @Get()
  async findAll(@Query() query: MoviesQuery): Promise<MovieEntity[]> {
    return await this.moviesService.findAll(query);
  }

  @Get(':id')
  async findOne(@Param('id') id: string): Promise<MovieEntity> {
    return await this.moviesService.findOne(id);
  }

  @UseGuards(AuthorizationGuard)
  @Patch(':id/:categoryName')
  @ApiOperation({ summary: 'Add category to a movie' })
  async addCategory(
    @Param('id') id: string,
    @Param('categoryName') categoryName: string,
  ): Promise<MovieEntity> {
    return await this.moviesService.addCategory(id, categoryName);
  }

  @UseGuards(AuthorizationGuard)
  @Patch(':id')
  async update(
    @Param('id') id: string,
    @Body() updateMovieDto: UpdateMovieDto,
  ): Promise<MovieEntity> {
    return await this.moviesService.update(id, updateMovieDto);
  }

  @UseGuards(AuthorizationGuard)
  @Delete(':id')
  async remove(@Param('id') id: string): Promise<MovieEntity> {
    return await this.moviesService.remove(id);
  }
}
