import { ApiHideProperty } from '@nestjs/swagger';
import { Movie } from '@prisma/client';

export class MovieEntity implements Movie {
  id: string;

  @ApiHideProperty()
  createdAt: Date;

  @ApiHideProperty()
  updatedAt: Date;

  @ApiHideProperty()
  deletedAt: Date;

  name: string;

  originalName: string;

  intro: string;

  picture: string | null;

  publishedAt: number;

  runTimeMinutes: number;

  directorId: string;
}
