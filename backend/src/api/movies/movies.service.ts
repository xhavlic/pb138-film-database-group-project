import { Injectable } from '@nestjs/common';
import { CreateMovieDto } from './dto/create-movie.dto';
import { UpdateMovieDto } from './dto/update-movie.dto';
import { PrismaService } from '../../prisma/prisma.service';
import { plainToClass } from 'class-transformer';
import { MovieEntity } from './entities/movie.entity';
import { Movie } from '@prisma/client';
import { MoviesQuery } from './dto/movies-query';

@Injectable()
export class MoviesService {
  constructor(private prisma: PrismaService) {}

  async create(createMovieDto: CreateMovieDto): Promise<Movie> {
    const movie = plainToClass(MovieEntity, createMovieDto);
    return await this.prisma.movie.create({
      data: movie,
    });
  }

  async findAll(query: MoviesQuery): Promise<Movie[]> {
    return await this.prisma.movie.findMany({
      where: {
        name: {
          contains: query.name,
          mode: 'insensitive',
        },
        originalName: {
          contains: query.origName,
          mode: 'insensitive',
        },
        runTimeMinutes: {
          gte: query.runtime?.gte,
          lte: query.runtime?.lte,
        },
        publishedAt: {
          gte: query.published?.gte,
          lte: query.published?.lte,
        },
        categories: {
          some: {
            id: {
              in: query.categoryIds,
            },
          },
        },
        directorId: {
          in: query.directorIds,
        },
      },
      orderBy: {
        publishedAt: 'desc',
      },
      take: query.mostRecentCnt,
      include: {
        director: true,
      },
    });
  }

  async findOne(id: string): Promise<Movie> {
    return await this.prisma.movie.findUnique({
      where: {
        id,
      },
      include: {
        director: true,
        categories: true,
      },
    });
  }

  async addCategory(movieId: string, categoryName: string): Promise<Movie> {
    await this.prisma.category.update({
      data: {
        movies: {
          connect: {
            id: movieId,
          },
        },
      },
      where: {
        name: categoryName,
      },
    });

    return await this.prisma.movie.update({
      data: {
        categories: {
          connect: {
            name: categoryName,
          },
        },
      },
      where: {
        id: movieId,
      },
      include: {
        categories: true,
      },
    });
  }

  async update(id: string, updateMovieDto: UpdateMovieDto): Promise<Movie> {
    const updatedMovie = {
      ...(await this.prisma.movie.findUnique({
        where: {
          id,
        },
      })),
      ...plainToClass(MovieEntity, updateMovieDto),
    };
    return await this.prisma.movie.update({
      where: {
        id,
      },
      data: {
        ...updatedMovie,
      },
    });
  }

  async remove(id: string): Promise<Movie> {
    return await this.prisma.movie.update({
      where: {
        id,
      },
      data: {
        deletedAt: new Date(),
      },
    });
  }
}
