import { Module } from '@nestjs/common';
import { ConfigModule } from '@nestjs/config';
import { CategoriesModule } from './api/categories/categories.module';
import { DirectorsModule } from './api/directors/directors.module';
import { MoviesModule } from './api/movies/movies.module';
import { AppController } from './app.controller';
import { AppService } from './app.service';
import { AuthorizationModule } from './authorization/authorization.module';

@Module({
  imports: [
    AuthorizationModule,
    ConfigModule.forRoot(),
    CategoriesModule,
    DirectorsModule,
    MoviesModule,
    AuthorizationModule,
  ],
  controllers: [AppController],
  providers: [AppService],
})
export class AppModule {}
