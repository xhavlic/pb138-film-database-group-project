import { ArgumentsHost, Catch, HttpStatus } from '@nestjs/common';
import { BaseExceptionFilter } from '@nestjs/core';
import { Prisma } from '@prisma/client';
import { Response } from 'express';

@Catch(Prisma.PrismaClientKnownRequestError)
export class PrismaClientExceptionFilter extends BaseExceptionFilter {
  catch(
    exception: Prisma.PrismaClientKnownRequestError,
    host: ArgumentsHost,
  ): void {
    const context = host.switchToHttp();
    const response = context.getResponse<Response>();

    switch (exception.code) {
      case 'P2002':
        // 409 error code when unique constraint violated
        const statusCode = HttpStatus.CONFLICT;
        const message = exception.message.replace(/\n/g, '');
        response.status(statusCode).json({
          statusCode,
          message, // TODO: check the message if it does not expose more info than necessary
        });
        break;
      // TODO: other exception codes?
      default:
        // default 500 error code
        super.catch(exception, host);
        break;
    }
  }
}
