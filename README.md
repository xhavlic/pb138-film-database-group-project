# PB138 - Film Database Group Project

***

## Name
Film Database

## Description
Filmová databáze obsahuje seznam filmů. Každý film má název, originální název, popisek, režiséra a kategorii. Filmy je možné vyhledat, procházet podle kategorie a nebo se z detailu dostat na filmy režiséra.

Film database containing movies. Each movie has name, original name, description, director and category. Movie search available, browsing categories and getting to director page through detail.

## Stack

Backend:
- NestJS

SSR:
- NextJS

Database technologies (ORM):
- Prisma

Frontend technologies:
- React

Source compiling:
- Vite

Infrastructure:
- dockercompose

UI:
- Ant
- Mantis


## Usage
### Docker
Firstly, run Docker daemon.

Navigate into the project root directory `pb138-film-database-group-project`.

Create your `.env` file here if you haven't done it yet. Example is in the `.env.example` file.

Then in the root directory run `docker-compose up -d` to build, (re)create, start, and attach to containers for a service (optional flag `-d` runs containers in the background).

When you are finished, run `docker-compose down` to stop and remove containers, networks and images created by `up`.

### Adminer
With Adminer we can manage our PostgreSQL database.

To login to Adminer in your browser you need to specify:
* System: `PostgreSQL`
* Server: `postgres` (docker-compose database service name)
  * if you changed `DB_PORT` in your .env file from 5432 (which is the default for Postgres), the server must be specified as `postgres:DB_PORT`
* Username: same as `DB_USER` in your .env file
* Password: same as `DB_PASSWORD` in your .env file
* Database: same as `DB_NAME` in your .env file


## REST API

| requirement |endpoint | method | criteria |
| ------------- | ------------- | ------------- | ------------- |
| Show all movies | /movies  | get | returns array of movies as a result of search
| Show most recent movies | /movies?mostRecentCnt=$COUNT  | get | returns array of $COUNT most recent movies
| Show a single movie | /movies/:id  | get | returns movie 
| Show a single director  | /directors/:id | get | returns director
| Show categories to browse | /categories | get | returns all categories
| Show a single category | /categories/:id | get | returns category with movies that belong to it

## TODO

More TBA as features are implemented.
