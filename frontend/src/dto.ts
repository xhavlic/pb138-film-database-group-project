export interface CategoryDTO {
  id: string;
  name: string;
  movies: MovieDTO[];
}

export interface MovieDTO {
  id: string;
  name: string;
  originalName: string;
  intro: string;
  picture: string;
  publishedAt: string;
  runTimeMinutes: number;
  director: DirectorDTO;
}

export interface DirectorDTO {
  id: string;
  name: string;
  surname: string;
  birthDate?: string;
  movies: MovieDTO[];
}
