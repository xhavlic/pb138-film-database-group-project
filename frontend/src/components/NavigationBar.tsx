import logo from "../LogoMovieBase.png";
import { Menu, Image } from "antd";
import { Header } from "antd/lib/layout/layout";
import { Link } from "react-router-dom";

// on top of the screen at all times, links to each feature of site
export const NavigationBar = () => {
  return (
    <div className="navigation-bar">
      <Header style={{display: "flex"}}>
        <Menu theme="dark" mode="horizontal" defaultSelectedKeys={["2"]}>
        <Link to="/">
          <Image src={logo} width={48} height={48} style={{width:"48", height: "48"}} preview={false} />
        </Link>
          <Menu.Item>
            <Link to="/">Home</Link>
          </Menu.Item>
          <Menu.Item>
            <Link to="/movies">Movies</Link>
          </Menu.Item>
          <Menu.Item>
            <Link to="/directors">Directors</Link>
          </Menu.Item>
          <Menu.Item>
            <Link to="/search">Search</Link>
          </Menu.Item>
          <Menu.Item>
            <Link to="/about">About</Link>
          </Menu.Item>
        </Menu>
      </Header>
    </div>
  );
};
