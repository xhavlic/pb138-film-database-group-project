import {
  Divider,
  Space,
} from "antd";
import React, { useEffect, useState } from "react";
import axiosConfig from "../axios-config";
import { MovieDTO } from "../dto";
import { ErrorPage } from "./ErrorPage";
import { PreviewBox } from "./PreviewBox";

export const Preview: React.FC = () => {
  const [moviesRecent, setMoviesRecent] = useState<MovieDTO[]>([]);
  const [movies, setMovies] = useState<MovieDTO[]>([]);
  const [isError, setIsError] = useState<boolean>(false);

  useEffect(() => {
    const fetchCategories = async () => {
      try {
        const response = await axiosConfig.get("movies?mostRecentCnt=12");
        const half = Math.ceil(response.data.length / 2);
        setMovies(response.data.slice(0, half));
        setMoviesRecent(response.data.slice(-half));
      } catch (err) {
        setIsError(true);
      }
    }

    void fetchCategories();
  }, [])

  if (isError) {
    return <ErrorPage />;
  }

  return (
    <Space
      className="preview"
      direction="vertical"
      style={{ display: "flex", paddingTop: "2%" }}
    >
      <PreviewBox title={"Editor's picks"} movies={moviesRecent} />
      <Divider/>
      <PreviewBox title={"Recent movies"} movies={movies} />
    </Space>
  );
}

export default Preview;
