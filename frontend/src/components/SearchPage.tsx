import { SearchOutlined } from "@ant-design/icons";
import {
  Button,
  Col,
  Divider,
  Form,
  Input,
  Row,
  Select,
  Slider,
  Space,
  Table,
} from "antd";
import { useEffect, useState } from "react";
import { Link } from "react-router-dom";
import axiosConfig from "../axios-config";
import { CategoryDTO, DirectorDTO, MovieDTO } from "../dto";
import { ErrorPage } from "./ErrorPage";

const { Option } = Select;

const runtimeMarks = {
  0: "0",
  30: "30",
  60: "60",
  90: "90",
  120: "120",
  150: "150",
  180: "180",
  210: "210",
  240: "240",
  270: "270",
  300: "300",
};

const publishedMarks = {
  1920: "1920",
  1940: "1940",
  1960: "1960",
  1980: "1980",
  2000: "2000",
  2020: "2020",
};

interface IFormProps {
  name: string;
  originalName: string;
  runTimeMinutes: number[];
  publishedAt: number[];
  categoryIds: string;
  directorIds: string;
};

//for searching movies
export const SearchPage = () => {
  const [form] = Form.useForm();

  const [categories, setCategories] = useState<CategoryDTO[]>([]);
  const [directors, setDirectors] = useState<DirectorDTO[]>([]);

  const [resultMovies, setResultMovies] = useState<MovieDTO[]>([]);

  const [isSubmitError, setIsSubmitError] = useState<boolean>(false);

  const runtimeMin = 0;
  const runtimeMax = 300;
  const publishedMin = 1920;
  const publishedMax = new Date().getFullYear();

  useEffect(() => {
    const fetchData = async () => {
      const catResponsePromise = axiosConfig.get<CategoryDTO[]>('/categories');
      const dirResponsePromise = axiosConfig.get<DirectorDTO[]>('/directors');

      setCategories([ ...((await catResponsePromise).data) ]);
      setDirectors([ ...((await dirResponsePromise).data) ]);
    };

    void fetchData();
  }, []);

  const onSubmit = async (values: IFormProps) => {
    try {
      const response = await axiosConfig.get<MovieDTO[]>('/movies', {
        params: {
          name: values.name,
          origName: values.originalName,
          runtime: {
            gte: values.runTimeMinutes ? values.runTimeMinutes[0] : undefined,
            lte: values.runTimeMinutes ? values.runTimeMinutes[1] : undefined,
          },
          published: {
            gte: values.publishedAt ? values.publishedAt[0] : undefined,
            lte: values.publishedAt ? values.publishedAt[1] : undefined,
          },
          categoryIds: values.categoryIds,
          directorIds: values.directorIds,
        }
      });
      setResultMovies([ ...response.data ]);
      setIsSubmitError(false);
    } catch {
      setIsSubmitError(true);
    }
  };

  const onReset = () => {
    form.resetFields();
  };

  //columns stays the same
  const columns = [
    {
      title: "Name",
      dataIndex: "name",
      sorter: (a: MovieDTO, b: MovieDTO) => {
        if (a.name < b.name) {return -1;}
        if (a.name > b.name) {return 1;}
        return 0;
      },
      render: (text: string, record: MovieDTO) => (
        <Link to={`/movie/${record.id}`}>
          {record.name}
        </Link>
      ),
    },
    {
      title: "Original name",
      className: "original-name",
      dataIndex: "originalName",
      sorter: (a: MovieDTO, b: MovieDTO) => {
        if (a.originalName < b.originalName) {return -1;}
        if (a.originalName > b.originalName) {return 1;}
        return 0;
      },
    },
    {
      title: "Release Date",
      dataIndex: "publishedAt",
      sorter: (a: MovieDTO, b: MovieDTO) => {
        if (a.publishedAt < b.publishedAt) {return -1;}
        if (a.publishedAt > b.publishedAt) {return 1;}
        return 0;
      },
    },
    {
      title: "Runtime",
      dataIndex: "runTimeMinutes",
      sorter: (a: MovieDTO, b: MovieDTO) => a.runTimeMinutes - b.runTimeMinutes,
    },
    {
      title: "Director",
      dataIndex: ["director", "surname"],
      sorter: (a: MovieDTO, b: MovieDTO) => {
        //surname has higher priority than name
        if (a.director.surname < b.director.surname) {return -1;}
        if (a.director.surname > b.director.surname) {return 1;}

        if (a.director.name < b.director.name) {return -1;}
        if (a.director.name > b.director.name) {return 1;}
        return 0;
      },
      render: (text: string, record: MovieDTO) => (
        <Link to={`/director/${record.director.id}`}>
          {record.director.name} {record.director.surname}
        </Link>
      ),
    },
  ];

  return (
    <>
      <Space
        direction="vertical"
        style={{ display: "flex", padding: 24, textAlign: "center" }}
      >
        <Form form={form} onFinish={onSubmit}>
          <Row gutter={20}>
            <Col span={6}>
              <Form.Item name="name" label="Name">
                <Input></Input>
              </Form.Item>
            </Col>
            <Col span={6}>
              <Form.Item name="originalName" label="Original name">
                <Input></Input>
              </Form.Item>
            </Col>
          </Row>
          <Row gutter={20}>
            <Col span={12}>
              <Form.Item name="runTimeMinutes" label="Runtime minutes">
                <Slider
                  marks={runtimeMarks}
                  range={{ draggableTrack: true }}
                  min={runtimeMin}
                  max={runtimeMax}
                  step={10}
                  defaultValue={[runtimeMin, runtimeMax]}
                ></Slider>
              </Form.Item>
            </Col>
          </Row>
          <Row gutter={20}>
            <Col span={12}>
              <Form.Item name="publishedAt" label="Year published">
              <Slider
                marks={publishedMarks}
                range={{ draggableTrack: true }}
                min={publishedMin}
                max={publishedMax}
                defaultValue={[publishedMin, publishedMax]}
              ></Slider>
              </Form.Item>
            </Col>
            <Col></Col>
          </Row>
          <Row gutter={20}>
            <Col span={6}>
              <Form.Item name="categoryIds" label="Category">
              <Select
                showSearch
                mode="multiple"
                style={{ width: "100%" }}
                placeholder="Search or Select"
                optionFilterProp="children"
                filterOption={(input, option) =>
                  (option!.children as unknown as string).toLowerCase().includes(input.toLowerCase())
                }
                filterSort={(optionA, optionB) =>
                  (optionA!.children as unknown as string)
                    .toLowerCase()
                    .localeCompare((optionB!.children as unknown as string).toLowerCase())
                }
              >
                {categories.map((category: CategoryDTO) => {
                  return (
                    <Option key={category.id} value={category.id}>
                      {category.name}
                    </Option>
                  );
                })}
              </Select>
              </Form.Item>
            </Col>
            <Col span={6}>
              <Form.Item name="directorIds" label="Director">
              <Select
                showSearch
                mode="multiple"
                style={{ width: "100%" }}
                placeholder="Search or Select"
                optionFilterProp="children"
                filterOption={(input, option) =>
                  (option!.children as unknown as string).toLowerCase().includes(input.toLowerCase())
                }
                filterSort={(optionA, optionB) =>
                  (optionA!.children as unknown as string)
                    .toLowerCase()
                    .localeCompare((optionB!.children as unknown as string).toLowerCase())
                }
              >
                {directors.map((director: DirectorDTO) => {
                  return (
                    <Option key={director.id} value={director.id}>
                      {`${director.name} ${director.surname}`}
                    </Option>
                  );
                })}
              </Select>
              </Form.Item>
            </Col>
          </Row>
          <Row>
            <Col span={12}>
              <Divider></Divider>
            </Col>
          </Row>
          <Row gutter={20}>
            <Col span={6}>
              <Form.Item name="searchbutton">
              <Button
                htmlType="submit"
                style={{ width: "100%" }}
                type="primary"
                icon={<SearchOutlined />}
                size="large"
              >
                Search
              </Button>
              </Form.Item>
            </Col>
            <Col span={6}>
              <Form.Item name="reset">
              <Button
                size="large"
                type="default"
                style={{ width: "100%" }}
                onClick={onReset}
              >
                Reset
              </Button>
              </Form.Item>
            </Col>
          </Row>
        </Form>
      </Space>
      <Space
        direction="vertical"
        style={{ display: "flex", padding: 24, textAlign: "left" }}
      >
        {isSubmitError
          ? <ErrorPage />
          : <Table
              columns={columns}
              dataSource={resultMovies}
              rowKey='id'
              bordered
              title={() => "Results"}
            />
        }
      </Space>
    </>
  );
};
