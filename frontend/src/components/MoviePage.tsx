import { Descriptions, Image } from "antd";
import { useEffect, useState } from "react";
import { Link, useParams } from "react-router-dom";
import axiosConfig from '../axios-config';
import {CategoryDTO, DirectorDTO, MovieDTO} from "../dto";
import { ErrorPage } from "./ErrorPage";

export const MoviePage = () => {
  const { id } = useParams();

  const [movie, setMovie] = useState<MovieDTO>({} as MovieDTO);
  const [director, setDirector] = useState<DirectorDTO>({} as DirectorDTO);
  const [categories, setCategories] = useState<CategoryDTO[]>([]);
  const [isError, setIsError] = useState<boolean>(false);


  useEffect(() => {
    const fetchMovie = async () => {
      try {
        const response = await axiosConfig.get("movies/" + id);
        setMovie(response.data);
        setDirector(response.data.director);
        setCategories(response.data.categories);
      } catch (err) {
        setIsError(true);
      }
    }

    void fetchMovie();
  }, [])

  if (isError) {
    return <ErrorPage />;
  }

  const movieCategories = categories.map((category) =>
      <>
          {category.name} &nbsp;&nbsp;
      </>
  );

  return (
    <>
      <Descriptions
        title={movie.name}
        bordered
        style={{ padding: "5%",}}
      >
        <Descriptions.Item label="Title picture" span={3}>
          <Image height={400} src={movie.picture}></Image>
        </Descriptions.Item>
        <Descriptions.Item label="Name" span={2}>
          {movie.name}
        </Descriptions.Item>
        <Descriptions.Item label="Original Name">
          {movie.originalName}
        </Descriptions.Item>
        <Descriptions.Item label="Description" span={3}>
          {movie.intro}
        </Descriptions.Item>
        <Descriptions.Item label="Published">
          {movie.publishedAt}
        </Descriptions.Item>
        <Descriptions.Item label="Runtime" span={2}>
          {movie.runTimeMinutes} minutes
        </Descriptions.Item>
        <Descriptions.Item label="Categories" span={3}>
          {movieCategories}
        </Descriptions.Item>
      </Descriptions>
      <Descriptions title="Director" bordered style={{ padding: "5%" }}>
        <Descriptions.Item label="Name" span={3}>
          <Link to={`/director/${director.id}`}>
            {director.name} {director.surname}
          </Link>
        </Descriptions.Item>
        <Descriptions.Item label="Birthdate">
          {director.birthDate}
        </Descriptions.Item>
      </Descriptions>
    </>
  );
};
