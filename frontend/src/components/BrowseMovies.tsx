import {
  Card,
  Collapse,
  Image,
  Space,
  List,
} from "antd";
import Meta from "antd/lib/card/Meta";
import { useEffect, useState } from "react";
import { Link } from "react-router-dom";
import { mainColor, middleColor, sideColor } from "../App";
import axiosConfig from "../axios-config";
import { CategoryDTO } from "../dto";
import { ErrorPage } from "./ErrorPage";

const { Panel } = Collapse;

// for browsing movies by categories
export const BrowseMovies = () => {
  const [categories, setCategories] = useState<CategoryDTO[]>([]);
  const [isError, setIsError] = useState<boolean>(false);

  useEffect(() => {
    const fetchCategories = async () => {
      try {
        const response = await axiosConfig.get("categories");
        setCategories(response.data);
      } catch (err) {
        setIsError(true);
      }
    };

    void fetchCategories();
  }, []);

  if (isError) {
    return <ErrorPage />
  }

  return (
    <div className="browse-container">
      <Space direction="vertical" style={{ padding: "3rem", display: "flex"}} >
        <Collapse style={{background: sideColor}}>
          {categories.map((category) => {
            return (
              <Panel header={category.name} key={category.id}>
                
                  <List
                    grid={{
                      gutter: 4,
                      xs: 1,
                      sm: 2,
                      md: 2,
                      lg: 2,
                      xl: 2,
                      xxl: 6,
                    }}
                    dataSource={category.movies}
                    rowKey="id"
                    pagination={{
                      showSizeChanger: false,
                      responsive: true,
                      pageSize: 6
                    }}
                    renderItem={(movie) => (
                      <List.Item>
                        <Space
                          style={{
                            display: "flex",
                            justifyContent: "space-between",
                          }}
                        >
                          <Card
                            style={{ width: "16rem" }}
                            cover={
                              <Image
                                width={"16rem"}
                                style={{ objectFit: "cover"}}
                                src={movie.picture}
                              ></Image>
                            }
                          >
                            <Link to={`/movie/${movie.id}`}>
                              <Meta
                                title={movie.name}
                                description={
                                  <div>
                                    {movie.originalName}
                                    <br></br>
                                    {movie.publishedAt}
                                  </div>
                                }
                              ></Meta>
                            </Link>
                          </Card>
                        </Space>
                      </List.Item>
                    )}
                  />
              </Panel>
            );
          })}
        </Collapse>
      </Space>
    </div>
  );
};
