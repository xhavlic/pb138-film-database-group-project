import { Descriptions } from "antd";
import axiosConfig from '../axios-config';
import { useEffect, useState } from "react";
import {Link, useParams} from "react-router-dom";
import { ErrorPage } from "./ErrorPage";
import {DirectorDTO, MovieDTO} from "../dto";

export const DirectorPage = () => {
  const { id: directorId } = useParams();
  const [director, setDirector] = useState<DirectorDTO>({} as DirectorDTO);
  const [movies, setMovies] = useState<MovieDTO[]>([]);
  const [isError, setIsError] = useState<boolean>(false);

  useEffect(() => {
    const fetchData = async () => {
      try {
        const response = await axiosConfig.get<DirectorDTO>(`/directors/${directorId}`);
        setDirector({ ...response.data })
        setMovies(response.data.movies);
      } catch {
        setIsError(true);
      }
    };

    void fetchData();
  }, []);

  if (isError) {
    return <ErrorPage />;
  }

  const directorMovies = movies.map((movie) =>
      <li>
        <Link to={`/movie/${movie.id}`}>
          {movie.name}
        </Link>
      </li>
  );

  return (
    <Descriptions title="Director page" bordered style={
        {padding: "4%", width:"50%"}
    }>
      <Descriptions.Item label="Name" span={3}>
        {director.name}
      </Descriptions.Item>
      <Descriptions.Item label="Surname" span={3}>
        {director.surname}
      </Descriptions.Item>
      <Descriptions.Item label="Movies" span={3}>
        {directorMovies}
      </Descriptions.Item>
      <Descriptions.Item label="Birthdate">
        {director.birthDate}
      </Descriptions.Item>
    </Descriptions>
  );
};
