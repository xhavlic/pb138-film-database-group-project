import { Space, Typography, Card } from "antd";

const { Title, Paragraph } = Typography;

export const About = () => {
  return (
    <Typography>
      <Space
        direction="vertical"
        size="middle"
        style={{ display: "flex", padding: 16, textAlign: "center" }}
      >
        <Card title={<Title>About</Title>}>
          <Paragraph> Movie Base is database of movies.</Paragraph>
          <Paragraph>
            It is possible to view popular movies, search for your favourites
            or checkout details of every movie in existence (or at least in our database)
          </Paragraph>
          <Paragraph>Authors: Lukáš Kratochvíl, Martin Korec, Tomáš Havlíček</Paragraph>
        </Card>
      </Space>
    </Typography>
  );
};
