import { Layout } from "antd";
import "antd/dist/antd.css";
import Preview from "./Preview";

const { Content, Footer } = Layout;

const MainPage = () => (
  <Layout className="layout">
    <Content
      style={{
        padding: "0 50px",
      }}
    >
      <Preview></Preview>
    </Content>
    <Footer
      style={{
        textAlign: "center",
      }}
    >
    </Footer>
  </Layout>
);

export default MainPage;
