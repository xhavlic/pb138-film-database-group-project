import { Space, Spin, Table } from "antd";
import { useEffect, useState } from "react";
import { Link } from "react-router-dom";
import axiosConfig from "../axios-config";
import { DirectorDTO } from "../dto";
import { ErrorPage } from "./ErrorPage";

export const BrowseDirectors = () => {
  const [directors, setDirectors] = useState<DirectorDTO[]>();
  const [isLoading, setIsLoading] = useState<boolean>(false);
  const [isError, setIsError] = useState<boolean>(false);

  useEffect(() => {
    const fetchData = async () => {
      setIsLoading(true);

      try {
        const response = await axiosConfig.get<DirectorDTO[]>('/directors');
        setDirectors(
          response.data.map((director) => ({
            ...director,
            id: director.id,
          }))
        );
      } catch {
        setIsError(true);
      }

      setIsLoading(false);
    };

    void fetchData();
  }, []);

  const columns = [
    {
      title: "Name",
      dataIndex: "name",
      sorter: (a: DirectorDTO, b: DirectorDTO) => {
        //surname has higher priority than name
        if (a.surname < b.surname) {return -1;}
        if (a.surname > b.surname) {return 1;}
        
        if (a.name < b.name) {return -1;}
        if (a.name > b.name) {return 1;}
        return 0;
      },
      render: (text: string, record: DirectorDTO) => (
        <Link to={`/director/${record.id}`}>
          {record.name} {record.surname}
        </Link>
      ),
    },
    {
      title: "Birthdate",
      className: "birthDate",
      dataIndex: "birthDate",
      sorter: (a: DirectorDTO, b: DirectorDTO) => {
        const first = a?.birthDate?.split('.').reverse();
        const second = b?.birthDate?.split('.').reverse();

        if (first === undefined && second === undefined) { return 0; }
        if (first === undefined) { return -1;}
        if (second === undefined) { return 1;}

        // year
        if (first[0] > second[0]) {return 1;}
        if (first[0] < second[0]) {return -1;}

        // month
        if (first[1] > second[1]) {return 1;}
        if (first[1] < second[1]) {return -1;}

        // day
        if (first[2] > second[2]) {return 1;}
        if (first[2] < second[2]) {return -1;}
        return 0;
      },
    }
  ];

  if (isError) {
    return <ErrorPage />
  }

  return (
    <div className="browse-container">
      <Space direction="vertical" size="middle" style={{ padding: "4%" }}>
        {isLoading && (
          <Spin tip="Loading..." size="large"/>
        )}
        <Table
          columns={columns}
          dataSource={directors}
          rowKey="id"
        />
      </Space>
    </div>
  );
}
