import { Card, Col, Row } from "antd";
import Meta from "antd/lib/card/Meta";
import { Link } from "react-router-dom";
import { mainColor, middleColor, sideColor } from "../App";
import { MovieDTO } from "../dto";

interface IPreviewBoxProps {
  title: string;
  movies: MovieDTO[];
}

export const PreviewBox: React.FC<IPreviewBoxProps> = ({ title, movies }) => {
  
  
  return (
    <Card title={title}  style={{ borderWidth: 4, borderColor:mainColor, background: sideColor, borderRadius: "10px",}}>
      <Row justify="space-evenly" align="bottom" gutter={50}>
        {movies.map((movie) => {
          return (
            <Col flex={1}>
              <Link to={`/movie/${movie.id}`}>
                <Card
                  hoverable
                  style={{
                    width: "240px",
                    borderWidth: "2px",
                    borderRadius: "5px",
                    borderColor: middleColor
                  }}
                  cover={<img alt={movie.name} src={movie.picture} />}
                >
                  <Meta title={movie.name} description={movie.originalName} />
                </Card>
              </Link>
            </Col>
          );
        })}
      </Row>
    </Card>
  );
}
