import "antd/dist/antd.css";
import "./App.css";
import MainPage from "./components/MainPage";
import { Routes, Route } from "react-router-dom";
import { NavigationBar } from "./components/NavigationBar";
import { About } from "./components/About";
import { BrowseMovies } from "./components/BrowseMovies";
import { SearchPage } from "./components/SearchPage";
import { ErrorPage } from "./components/ErrorPage";
import { MoviePage } from "./components/MoviePage";
import { DirectorPage } from "./components/DirectorPage";
import { BrowseDirectors } from "./components/BrowseDirectors";

export const mainColor = "#69c0ff"
export const middleColor = "#bae7ff"
export const sideColor = "#e6f7ff"

function App() {
  return (
    <div className="app">
      <NavigationBar />

      <Routes>
        <Route path="/" element={<MainPage />}></Route>
        <Route path="/movies" element={<BrowseMovies />}></Route>
        <Route path="/directors" element={<BrowseDirectors/>}></Route>
        <Route path="/search" element={<SearchPage />}></Route>
        <Route path="/about" element={<About />}></Route>
        <Route path="/movie/:id" element={<MoviePage></MoviePage>}></Route>
        <Route path="/director/:id" element={<DirectorPage />}></Route>
        <Route path="*" element={<ErrorPage />}></Route>
      </Routes>
    </div>
  );
}

export default App;
