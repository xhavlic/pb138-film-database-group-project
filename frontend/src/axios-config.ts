import axios from 'axios';
import Qs from 'qs';

const instance = axios.create({
  baseURL: `http://127.0.0.1:${import.meta.env.VITE_SERVER_PORT}`,
  paramsSerializer: (params: any) => {
    return Qs.stringify(params, {
      arrayFormat: 'brackets',
      encode: false,
    });
  }
});

export default instance;
